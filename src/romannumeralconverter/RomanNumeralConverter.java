package romannumeralconverter;

/**
 *
 * @author Kurtis Saddler
 */
public class RomanNumeralConverter {

    /**
     * The main method is simply passing inputs to private methods where
     * calculations and transactions take place away from the "client side"
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        translateNumbers(args);
    }

    /**
     * Builds a string output to display the response for each output and shows
     * it on the command line
     */
    private static void translateNumbers(String[] inputs) {
        for (String decimalNumber : inputs) {
            if (Integer.valueOf(decimalNumber) >= 1 && Integer.valueOf(decimalNumber) <= 3999) {
                System.out.println(decimalNumber + " = \"" 
                        + convertToRomanNumerals(decimalNumber) + "\"");
            } else {
                System.out.println("Please submit an integer between 1 and 3999");
            }
        }
    }

    /**
     * This method determines the length of the string to allow the string to be
     * translated into Roman numerals in terms of units, tens, hundreds &
     * thousands
     *
     * @return the Roman numeral equivalent of the decimal number input
     */
    private static String convertToRomanNumerals(String decimalNumber) {
        String romanNumeral = "";
        int thousandsValue, hundredsValue, tensValue, unitsValue;

        switch (decimalNumber.length()) {
            case 1:
                romanNumeral = units(Integer.valueOf(decimalNumber));
                break;
            case 2:
                // Storing values of each column of the decimal number 
                // to make the code more readable. Unable to use charAt method
                // as it grabs the unicode value not the Integer value
                tensValue = Integer.valueOf(decimalNumber.substring(0, 1));
                unitsValue = Integer.valueOf(decimalNumber.substring(1, 2));
                romanNumeral = tens(tensValue) + units(unitsValue);
                break;
            case 3:
                // Storing values of each column of the decimal number 
                // to make the code more readable. Unable to use charAt method
                // as it grabs the unicode value not the Integer value
                hundredsValue = Integer.valueOf(decimalNumber.substring(0, 1));
                tensValue = Integer.valueOf(decimalNumber.substring(1, 2));
                unitsValue = Integer.valueOf(decimalNumber.substring(2, 3));
                romanNumeral = hundreds(hundredsValue) + tens(tensValue)
                        + units(unitsValue);
                break;
            case 4:
                // Storing values of each column of the decimal number 
                // to make the code more readable. Unable to use charAt method
                // as it grabs the unicode value not the Integer value
                thousandsValue = Integer.valueOf(decimalNumber.substring(0, 1));
                hundredsValue = Integer.valueOf(decimalNumber.substring(1, 2));
                tensValue = Integer.valueOf(decimalNumber.substring(2, 3));
                unitsValue = Integer.valueOf(decimalNumber.substring(3, 4));
                romanNumeral = thousands(thousandsValue) + hundreds(hundredsValue)
                        + tens(tensValue) + units(unitsValue);
                break;
            default:
                break;
        }
        return romanNumeral;
    }

    /**
     * Calculates the units column of the decimal number input
     *
     * @return the Roman numeral equivalent for the units column of the decimal
     * number input
     */
    private static String units(int n) {
        String output = "";

        if (n <= 3) {
            output = writeUnitsString(n);
        } else if (n == 4) {
            // Hard coded as a special circumstance seeing as there are
            // limitations on the span of the project
            output = "IV";
        } else if (n <= 8) {
            output = "V" + writeUnitsString(n - 5);
        } else if (n == 9) {
            // Hard coded as a special circumstance seeing as there are
            // limitations on the span of the project
            output = "IX";
        }

        return output;
    }

    /**
     * Calculates the tens column of the decimal number input
     *
     * @return the Roman numeral equivalent for the tens column of the decimal
     * number input
     */
    private static String tens(int n) {
        String output = "";

        if (n <= 3) {
            output = writeTensString(n);
        } else if (n == 4) {
            // Hard coded as a special circumstance seeing as there are
            // limitations on the span of the project
            output = "XL";
        } else if (n <= 8) {
            output = "L" + writeTensString(n - 5);
        } else if (n == 9) {
            // Hard coded as a special circumstance seeing as there are
            // limitations on the span of the project
            output = "XC";
        }

        return output;
    }

    /**
     * Calculates the hundreds column of the decimal number input
     *
     * @return the Roman numeral equivalent for the hundreds column of the
     * decimal number input
     */
    private static String hundreds(int n) {
        String output = "";

        if (n <= 3) {
            output = writeHundredsString(n);
        } else if (n == 4) {
            // Hard coded as a special circumstance seeing as there are
            // limitations on the span of the project
            output = "CD";
        } else if (n <= 8) {
            output = "D" + writeHundredsString(n - 5);
        } else if (n == 9) {
            // Hard coded as a special circumstance seeing as there are
            // limitations on the span of the project
            output = "CM";
        }

        return output;
    }

    /**
     * Calculates the thousands column of the decimal number input, no need to
     * handle special circumstances as the project is limited at 3999, first
     * special circumstance would come at 4000 which would require a change in
     * the original specification and therefore the product delivered
     *
     * @return the Roman numeral equivalent for the thousands column of the
     * decimal number input
     */
    private static String thousands(int n) {
        String s = "";

        for (int i = 0; i < n; i++) {
            s = s.concat("M");
        }

        return s;
    }

    /**
     * Calculates the number of "I" characters to add to the Roman numeral
     * representation of the decimal number
     *
     * @return string of "I" characters to match the remainder of the decimal
     * number that's not covered by another Roman numeral
     */
    private static String writeUnitsString(int n) {
        String s = "";

        for (int i = 0; i < n; i++) {
            s = s.concat("I");
        }

        return s;
    }

    /**
     * Calculates the number of "X" characters to add to the Roman numeral
     * representation of the decimal number
     *
     * @return string of "X" characters to match the remainder of the decimal
     * number that's not covered by another Roman numeral
     */
    private static String writeTensString(int n) {
        String s = "";

        for (int i = 0; i < n; i++) {
            s = s.concat("X");
        }

        return s;
    }

    /**
     * Calculates the number of "C" characters to add to the Roman numeral
     * representation of the decimal number
     *
     * @return string of "C" characters to match the remainder of the decimal
     * number that's not covered by another Roman numeral
     */
    private static String writeHundredsString(int n) {
        String s = "";

        for (int i = 0; i < n; i++) {
            s = s.concat("C");
        }

        return s;
    }

}